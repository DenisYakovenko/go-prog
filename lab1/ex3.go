package main

import "fmt"

func main() {
	var userinit8 uint8 = 1
	var userinit16 uint16 = 2
	var userinit64 int64 = -3
	var userautoinit = -4

	fmt.Println("Values: ", userinit8, userinit16, userinit64, userautoinit, "\n")

	fmt.Printf("Type of userinit8 = %T\n", userinit8)
	fmt.Printf("Type of userinit16 = %T\n", userinit16)
	fmt.Printf("Type of userinit64 = %T\n", userinit64)
	fmt.Printf("Type of userautoinit = %T\n", userautoinit)

	intVar := 10

	fmt.Printf("Value = %d Type = %T\n", intVar, intVar)

	intVar = int(userinit16) // Приведення типу
	fmt.Printf("New value of intVar = %d\n", intVar)

	intVar = userautoinit // Типи збігаються, приведення типу не потрібне
	fmt.Printf("New value of intVar = %d\n", intVar)
}
