package main

import "fmt"

func main() {
	var x, y, z uint8

	x = 9
	y = 28
	z = x

	fmt.Println("Битовые операции")

	fmt.Printf("^x      = ^(%d)      = ^(%.8b)            = %.8b = %d\n", x, x, ^x, ^x)
	fmt.Printf("x << 2  = (%d << 2)  = (%.8b << 2)        = %.8b = %d\n", x, x, x<<2, x<<2)
	fmt.Printf("x >> 2  = (%d >> 2)  = (%.8b >> 2)        = %.8b = %d\n", x, x, x>>2, x>>2)
	fmt.Printf("x & y   = (%d & %d)  = (%.8b & %.8b)  = %.8b = %d\n", x, y, x, y, x&y, x&y)
	fmt.Printf("x | y   = (%d | %d)  = (%.8b | %.8b)  = %.8b = %d\n", x, y, x, y, x|y, x|y)
	fmt.Printf("x ^ y   = (%d ^ %d)  = (%.8b ^ %.8b)  = %.8b = %d\n", x, y, x, y, x^y, x^y)
	fmt.Printf("x &^ y  = (%d &^ %d) = (%.8b &^ %.8b) = %.8b = %d\n", x, y, x, y, x&^y, x&^y)
	fmt.Printf("x %% y   = (%d %% %d)  = (%.8b %% %.8b)  = %.8b = %d\n", x, y, x, y, x%y, x%y)

	fmt.Println("\nБитовые операции с присваиванием")

	x = z
	x &= y
	fmt.Printf("x &= y   = (%d &= %d)  = (%.8b &= %.8b)  = %.8b = %d\n", z, y, z, y, x, x)
	x = z
	x |= y
	fmt.Printf("x |= y   = (%d |= %d)  = (%.8b |= %.8b)  = %.8b = %d\n", z, y, z, y, x, x)
	x = z
	x ^= y
	fmt.Printf("x ^= y   = (%d ^= %d)  = (%.8b ^= %.8b)  = %.8b = %d\n", z, y, z, y, x, x)
	x = z
	x &^= y
	fmt.Printf("x &^= y  = (%d &^= %d) = (%.8b &^= %.8b) = %.8b = %d\n", z, y, z, y, x, x)
	x = z
	x %= y
	fmt.Printf("x %%= y   = (%d %%= %d)  = (%.8b %%= %.8b)  = %.8b = %d\n", z, y, z, y, x, x)

	//Задание.
	//Побитовое отрицание (^)
	//Операция ^x инвертирует каждый бит переменной x. В вашем случае x = 9, в двоичной системе 0000 1001. Инвертирование каждого бита даст 1111 0110, что равно 246 в десятичной системе.
	//
	//Сдвиг влево (<<)
	//Операция x << 2 сдвигает все биты переменной x на две позиции влево. В двоичной системе x = 0000 1001, сдвиг влево на 2 бита даст 0010 0100, что равно 36 в десятичной системе.
	//
	//Сдвиг вправо (>>)
	//Операция x >> 2 сдвигает все биты переменной x на две позиции вправо. В двоичной системе x = 0000 1001, сдвиг вправо на 2 бита даст 0000 0010, что равно 2 в десятичной системе.
	//
	//Побитовое И (&)
	//Операция x & y выполняет побитовое И между x и y. Это означает, что каждый бит результата является И (AND) соответствующих битов x и y.
	//
	//Побитовое ИЛИ (|)
	//Операция x | y выполняет побитовое ИЛИ между x и y.
	//
	//Побитовое исключающее ИЛИ (^)
	//Операция x ^ y выполняет побитовое исключающее ИЛИ между x и y.
	//
	//Побитовое И-НЕ (&^)
	//Операция x &^ y устанавливает каждый бит результата в 1, если соответствующий бит y равен 0.
	//
	//Остаток от деления (%)
	//Операция x % y возвращает остаток от деления x на y.
	//
	//Битовые операции с присваиванием
	//Вы также демонстрируете битовые операции с присваиванием (&=, |=, ^=, &^=, %=), которые являются сокращенными формами соответствующих операций.

}
