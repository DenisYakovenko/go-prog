package main

import "fmt"

func main() {
	variable8 := int8(127)
	variable16 := int16(16383)

	fmt.Println("Приведение типов\n")

	fmt.Printf("variable8         = %-5d = %.16b\n", variable8, variable8)
	fmt.Printf("variable16        = %-5d = %.16b\n", variable16, variable16)
	fmt.Printf("uint16(variable8) = %-5d = %.16b\n", uint16(variable8), uint16(variable8))
	fmt.Printf("uint8(variable16) = %-5d = %.16b\n", uint8(variable16), uint8(variable16))

	// Задание 1: Создайте 2 переменные разных типов. Выпоните арифметические операции. Результат вывести.
	var a float32 = 5.5
	var b int = 2

	// Приводим тип переменной b к float32 перед выполнением операции
	sum := a + float32(b)
	diff := a - float32(b)
	prod := a * float32(b)
	quot := a / float32(b)

	fmt.Printf("\nАрифметические операции:\n")
	fmt.Printf("a + b = %.2f\n", sum)
	fmt.Printf("a - b = %.2f\n", diff)
	fmt.Printf("a * b = %.2f\n", prod)
	fmt.Printf("a / b = %.2f\n", quot)
}
