package main

import (
	"fmt"
	"math"
)

func main() {
	var defaultFloat float32
	var defaultDouble float64 = 5.5

	fmt.Println("defaultfloat       = ", defaultFloat)
	fmt.Printf("defaultDouble (%T) = %f\n\n", defaultDouble, defaultDouble)

	fmt.Println("MAX float32        = ", math.MaxFloat32)
	fmt.Println("MIN float32        = ", math.SmallestNonzeroFloat32, "\n")

	fmt.Println("MAX float64        = ", math.MaxFloat64)
	fmt.Println("MIN float64        = ", math.SmallestNonzeroFloat64, "\n")

	// Задание 1: Создайте переменные разных типов, используя краткую запись и инициализацию по-умолчанию
	myInt := 42                // int
	myFloat := 3.14159         // float64
	myBool := true             // bool
	myString := "Hello, Go!"   // string
	myComplex := complex(1, 2) // complex128

	fmt.Println("myInt:", myInt)
	fmt.Println("myFloat:", myFloat)
	fmt.Println("myBool:", myBool)
	fmt.Println("myString:", myString)
	fmt.Println("myComplex:", myComplex)
}
