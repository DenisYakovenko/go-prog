package main

import "fmt"

func main() {
	var first, second bool
	var third bool = true
	fourth := !third
	var fifth = true

	fmt.Println("first  = ", first)       // false
	fmt.Println("second = ", second)      // false
	fmt.Println("third  = ", third)       // true
	fmt.Println("fourth = ", fourth)      // false
	fmt.Println("fifth  = ", fifth, "\n") // true

	fmt.Println("!true  = ", !true)        // false
	fmt.Println("!false = ", !false, "\n") // true

	fmt.Println("true && true   = ", true && true)         // true
	fmt.Println("true && false  = ", true && false)        // false
	fmt.Println("false && false = ", false && false, "\n") // false

	fmt.Println("true || true   = ", true || true)         // true
	fmt.Println("true || false  = ", true || false)        // true
	fmt.Println("false || false = ", false || false, "\n") // false

	fmt.Println("2 < 3  = ", 2 < 3)        // true
	fmt.Println("2 > 3  = ", 2 > 3)        // false
	fmt.Println("3 < 3  = ", 3 < 3)        // false
	fmt.Println("3 <= 3 = ", 3 <= 3)       // true
	fmt.Println("3 > 3  = ", 3 > 3)        // false
	fmt.Println("3 >= 3 = ", 3 >= 3)       // true
	fmt.Println("2 == 3 = ", 2 == 3)       // false
	fmt.Println("3 == 3 = ", 3 == 3)       // true
	fmt.Println("2 != 3 = ", 2 != 3)       // true
	fmt.Println("3 != 3 = ", 3 != 3, "\n") // false

	//Задание.
	//1. Пояснить результаты операций
	//Логические операции
	//!true — логическое отрицание. Возвращает false, так как отрицание true это false.
	//!false — логическое отрицание. Возвращает true, так как отрицание false это true.
	//true && true — логическое И. Возвращает true, так как оба операнда являются true.
	//true && false — логическое И. Возвращает false, так как один из операндов false.
	//false && false — логическое И. Возвращает false, так как оба операнда false.
	//true || true — логическое ИЛИ. Возвращает true, так как хотя бы один операнд true.
	//true || false — логическое ИЛИ. Возвращает true, так как хотя бы один операнд true.
	//false || false — логическое ИЛИ. Возвращает false, так как оба операнда false.
	//Сравнительные операции
	//2 < 3 — возвращает true, так как 2 действительно меньше 3.
	//2 > 3 — возвращает false, так как 2 не больше 3.
	//3 < 3 — возвращает false, так как числа равны.
	//3 <= 3 — возвращает true, так как 3 равно 3.
	//3 > 3 — возвращает false, так как числа равны.
	//3 >= 3 — возвращает true, так как 3 равно 3.
	//2 == 3 — возвращает false, так как числа не равны.
	//3 == 3 — возвращает true, так как числа равны.
	//2 != 3 — возвращает true, так как числа не равны.
	//3 != 3 — возвращает false, так как числа равны.
}
