package main

// знаходження мінімального значення з трьох елементів
func findMin(a, b, c int) int {
	if a < b {
		if a < c {
			return a
		}
		return c
	}
	if b < c {
		return b
	}
	return c
}

// обчислення середнього значення трьох елементів
func average(a, b, c int) float64 {
	return float64(a+b+c) / 3.0
}

// рішення рівняння першого порядку ax + b = 0
func solveEquation(a, b int) (float64, bool) {
	if a == 0 {
		if b == 0 {
			return 0, false // неоднозначне рішення (рівняння виконується для будь-якого x)
		}
		return 0, true // рівняння не має рішення
	}
	return float64(-b) / float64(a), true // рішення рівняння
}
