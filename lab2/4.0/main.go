package main

import "fmt"

func main() {
	// Тестування функцій
	fmt.Println(findMin(3, 1, 4))    // виведе: 1
	fmt.Println(average(3, 1, 4))    // виведе: 2.6666666666666665
	x, exists := solveEquation(2, 4) // рівняння 2x + 4 = 0
	if exists {
		fmt.Println(x) // виведе: -2
	} else {
		fmt.Println("No unique solution")
	}
}
