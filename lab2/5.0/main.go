package main

import (
	"5.0/mymath"
	"fmt"
)

func main() {
	fmt.Println(mymath.FindMin(3, 1, 4))    // виведе: 1
	fmt.Println(mymath.Average(3, 1, 4))    // виведе: 2.6666666666666665
	x, exists := mymath.SolveEquation(2, 4) // рівняння 2x + 4 = 0
	if exists {
		fmt.Println(x) // виведе: -2
	} else {
		fmt.Println("No unique solution")
	}
}
