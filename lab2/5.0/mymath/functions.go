package mymath

// FindMin знаходить мінімальне значення з трьох елементів.
func FindMin(a, b, c int) int {
	if a < b {
		if a < c {
			return a
		}
		return c
	}
	if b < c {
		return b
	}
	return c
}

// Average обчислює середнє значення трьох елементів.
func Average(a, b, c int) float64 {
	return float64(a+b+c) / 3.0
}

// SolveEquation рішення рівняння першого порядку ax + b = 0.
func SolveEquation(a, b int) (float64, bool) {
	if a == 0 {
		if b == 0 {
			return 0, false // неоднозначне рішення (рівняння виконується для будь-якого x)
		}
		return 0, true // рівняння не має рішення
	}
	return float64(-b) / float64(a), true // рішення рівняння
}
