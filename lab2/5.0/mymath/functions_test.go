package mymath

import "testing"

func TestFindMin(t *testing.T) {
	tests := []struct {
		a, b, c, want int
	}{
		{1, 2, 3, 1},
		{3, 2, 1, 1},
		{3, 3, 3, 3},
	}

	for _, test := range tests {
		if got := FindMin(test.a, test.b, test.c); got != test.want {
			t.Errorf("FindMin(%d, %d, %d) = %d; want %d", test.a, test.b, test.c, got, test.want)
		}
	}
}

func TestAverage(t *testing.T) {
	tests := []struct {
		a, b, c int
		want    float64
	}{
		{1, 2, 3, 2.0},
		{3, 2, 1, 2.0},
		{3, 3, 3, 3.0},
	}

	for _, test := range tests {
		if got := Average(test.a, test.b, test.c); got != test.want {
			t.Errorf("Average(%d, %d, %d) = %f; want %f", test.a, test.b, test.c, got, test.want)
		}
	}
}

func TestSolveEquation(t *testing.T) {
	tests := []struct {
		a, b    int
		wantX   float64
		wantSol bool
	}{
		{2, 4, -2, true},
		{0, 4, 0, true},
		{0, 0, 0, false},
	}

	for _, test := range tests {
		gotX, gotSol := SolveEquation(test.a, test.b)
		if gotX != test.wantX || gotSol != test.wantSol {
			t.Errorf("SolveEquation(%d, %d) = (%f, %t); want (%f, %t)", test.a, test.b, gotX, gotSol, test.wantX, test.wantSol)
		}
	}
}
