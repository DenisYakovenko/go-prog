package main

import (
	"fmt"
	"math"
)

// Параметри для конгруентного методу
const (
	a = 16807
	c = 0
	m = 1<<31 - 1
	n = 200
	K = 30000
)

// Генерація псевдовипадкових чисел
func generatePseudoRandomNumbers(seed int64, length int) []int {
	numbers := make([]int, length)
	for i := 0; i < length; i++ {
		seed = (a*seed + c) % m
		numbers[i] = int(seed % n)
	}
	return numbers
}

// Розрахунок частоти інтервалів
func calculateFrequency(numbers []int) map[int]int {
	frequency := make(map[int]int)
	for _, number := range numbers {
		frequency[number]++
	}
	return frequency
}

// Розрахунок математичного сподівання
func calculateMean(numbers []int) float64 {
	sum := 0
	for _, number := range numbers {
		sum += number
	}
	return float64(sum) / float64(len(numbers))
}

// Розрахунок дисперсії
func calculateVariance(numbers []int, mean float64) float64 {
	var sum float64
	for _, number := range numbers {
		sum += math.Pow(float64(number)-mean, 2)
	}
	return sum / float64(len(numbers))
}

// Основна функція
func main() {
	seed := int64(1) // Початкове значення
	numbers := generatePseudoRandomNumbers(seed, K)

	frequency := calculateFrequency(numbers)
	fmt.Println("Frequency of intervals:", frequency)

	mean := calculateMean(numbers)
	fmt.Printf("Mean (Mathematical Expectation): %.2f\n", mean)

	variance := calculateVariance(numbers, mean)
	fmt.Printf("Variance: %.2f\n", variance)

	stdDeviation := math.Sqrt(variance)
	fmt.Printf("Standard Deviation: %.2f\n", stdDeviation)
}
