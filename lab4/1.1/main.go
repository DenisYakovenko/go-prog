package main

import (
	"fmt"
	"github.com/andlabs/ui"
	"strconv"
)

// Визначення вартості за параметрами
func EstimateCost(width, height int, glassType, frameMaterial string, includeSill bool) float64 {
	costPerUnit := map[string]map[string]float64{
		"SinglePane": {
			"Wood":  2.5,
			"Metal": 0.5,
			"PVC":   1.5,
		},
		"DoublePane": {
			"Wood":  3.0,
			"Metal": 1.0,
			"PVC":   2.0,
		},
	}

	cost := float64(width*height) * costPerUnit[glassType][frameMaterial]
	if includeSill {
		cost += 350.0
	}
	return cost
}

func main() {
	ui.Main(func() {
		mainWindow := ui.NewWindow("Glass Package Cost Estimator", 400, 400, false)
		layout := ui.NewVerticalBox()
		layout.SetPadded(true)

		widthLayout := ui.NewHorizontalBox()
		widthLayout.SetPadded(true)
		widthLabel := ui.NewLabel("Width (cm):")
		widthInput := ui.NewEntry()

		heightLayout := ui.NewHorizontalBox()
		heightLayout.SetPadded(true)
		heightLabel := ui.NewLabel("Height (cm):")
		heightInput := ui.NewEntry()

		widthLayout.Append(widthLabel, false)
		widthLayout.Append(widthInput, true)
		heightLayout.Append(heightLabel, false)
		heightLayout.Append(heightInput, true)

		glassSelector := ui.NewCombobox()
		glassSelector.Append("SinglePane")
		glassSelector.Append("DoublePane")

		frameSelector := ui.NewCombobox()
		frameSelector.Append("Wood")
		frameSelector.Append("Metal")
		frameSelector.Append("PVC")

		sillOption := ui.NewCheckbox("Include windowsill?")
		calculateBtn := ui.NewButton("Calculate")
		resultOutput := ui.NewLabel("")

		layout.Append(widthLayout, false)
		layout.Append(heightLayout, false)
		layout.Append(glassSelector, false)
		layout.Append(frameSelector, false)
		layout.Append(sillOption, false)
		layout.Append(calculateBtn, false)
		layout.Append(resultOutput, false)

		mainWindow.SetChild(layout)

		calculateBtn.OnClicked(func(*ui.Button) {
			width, _ := strconv.Atoi(widthInput.Text())
			height, _ := strconv.Atoi(heightInput.Text())

			glassType := "SinglePane"
			if glassSelector.Selected() == 1 {
				glassType = "DoublePane"
			}

			frameMaterial := []string{"Wood", "Metal", "PVC"}[frameSelector.Selected()]
			includeSill := sillOption.Checked()

			cost := EstimateCost(width, height, glassType, frameMaterial, includeSill)
			resultOutput.SetText(fmt.Sprintf("Cost: %.2f UAH", cost))
		})

		mainWindow.OnClosing(func(*ui.Window) bool {
			ui.Quit()
			return true
		})

		mainWindow.Show()
	})
}

// Спрощена функція конвертації рядка в число
func parseToInt(str string) int {
	result, err := strconv.Atoi(str)
	if err != nil {
		return 0 // Повертаємо 0 у випадку помилки
	}
	return result
}
