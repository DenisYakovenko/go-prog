#include <stdbool.h>
#include <string.h>

double calculate_price(int width, int height, const char* typeOfGlass, const char* frame, bool windowsill) {
    double prices[2][3] = {
        {2.5, 0.5, 1.5},  // Однокамерний
        {3.0, 1.0, 2.0}  // Двокамерний
    };

    int typeIndex = (strcmp(typeOfGlass, "Двокамерний") == 0) ? 1 : 0;
    int frameIndex;
    if (strcmp(frame, "Дерев'яний") == 0) frameIndex = 0;
    else if (strcmp(frame, "Металевий") == 0) frameIndex = 1;
    else frameIndex = 2;

    double price = width * height * prices[typeIndex][frameIndex];
    if (windowsill) {
        price += 350.0;
    }
    return price;
}
