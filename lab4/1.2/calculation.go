package main

// #cgo CFLAGS: -I.
// #cgo LDFLAGS: -L. -lcalculation
// #include "calculation.h"
import "C"

func CalculatePriceC(width, height int, typeOfGlass, frame string, windowsill bool) float64 {
	cTypeOfGlass := C.CString(typeOfGlass)
	cFrame := C.CString(frame)
	price := C.calculate_price(C.int(width), C.int(height), cTypeOfGlass, cFrame, C.bool(windowsill))
	return float64(price)
}
