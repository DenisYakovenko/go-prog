package main

import (
	"fmt"
	"github.com/andlabs/ui"
	_ "github.com/andlabs/ui/winmanifest"
	"strconv"
)

// import "./path_to_your_c_wrapper"

func main() {
	ui.Main(func() {
		window := ui.NewWindow("Розрахунок вартості склопакета", 400, 400, false)

		vbox := ui.NewVerticalBox()
		vbox.SetPadded(true)

		hbox := ui.NewHorizontalBox()
		hbox.SetPadded(true)

		hbox.Append(ui.NewLabel("Ширина (см):"), false)
		widthEntry := ui.NewEntry()
		hbox.Append(widthEntry, true)

		vbox.Append(hbox, false)

		hbox = ui.NewHorizontalBox()
		hbox.SetPadded(true)

		hbox.Append(ui.NewLabel("Висота (см):"), false)
		heightEntry := ui.NewEntry()
		hbox.Append(heightEntry, true)

		vbox.Append(hbox, false)

		typesOfGlass := ui.NewCombobox()
		typesOfGlass.Append("Однокамерний")
		typesOfGlass.Append("Двокамерний")
		vbox.Append(typesOfGlass, false)

		frames := ui.NewCombobox()
		frames.Append("Дерев'яний")
		frames.Append("Металевий")
		frames.Append("Металопластиковий")
		vbox.Append(frames, false)

		windowsillCheckbox := ui.NewCheckbox("Додати підвіконня?")
		vbox.Append(windowsillCheckbox, false)

		button := ui.NewButton("Розрахувати")
		vbox.Append(button, false)

		resultLabel := ui.NewLabel("")
		vbox.Append(resultLabel, false)

		window.SetChild(vbox)

		button.OnClicked(func(*ui.Button) {
			width := atoi(widthEntry.Text())
			height := atoi(heightEntry.Text())

			typeIndex := typesOfGlass.Selected()
			frameIndex := frames.Selected()

			typeOfGlass := ""
			if typeIndex == 0 {
				typeOfGlass = "Однокамерний"
			} else {
				typeOfGlass = "Двокамерний"
			}

			frame := ""
			if frameIndex == 0 {
				frame = "Дерев'яний"
			} else if frameIndex == 1 {
				frame = "Металевий"
			} else {
				frame = "Металопластиковий"
			}

			windowsill := windowsillCheckbox.Checked()

			// Використання C-функції для розрахунку
			price := CalculatePriceC(width, height, typeOfGlass, frame, windowsill)
			resultLabel.SetText(fmt.Sprintf("Вартість: %.2f грн", price))
		})

		window.OnClosing(func(*ui.Window) bool {
			ui.Quit()
			return true
		})

		window.Show()
	})
}

func atoi(s string) int {
	val, _ := strconv.Atoi(s)
	return val
}
