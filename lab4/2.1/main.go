package main

import (
	"fmt"
	"github.com/andlabs/ui"
	_ "github.com/andlabs/ui/winmanifest"
)

func CalculateTourPrice(destination, season string, guide bool, luxury bool) float64 {
	pricePerDay := map[string]map[string]float64{
		"Болгарія": {
			"літо": 100.0,
			"зима": 150.0,
		},
		"Німеччина": {
			"літо": 160.0,
			"зима": 200.0,
		},
		"Польща": {
			"літо": 120.0,
			"зима": 180.0,
		},
	}

	basePrice := pricePerDay[destination][season]

	if guide {
		basePrice += 50.0
	}

	if luxury {
		basePrice *= 1.2
	}

	return basePrice
}

func main() {
	err := ui.Main(func() {
		window := ui.NewWindow("Розрахунок вартості туру", 400, 200, false)
		window.SetMargined(true)

		vbox := ui.NewVerticalBox()
		vbox.SetPadded(true)

		// Місце призначення
		destinationOptions := []string{"Болгарія", "Німеччина", "Польща"}
		destinationComboBox := ui.NewCombobox()
		for _, option := range destinationOptions {
			destinationComboBox.Append(option)
		}
		destinationComboBox.SetSelected(0) // Задаємо значення за замовчуванням
		destinationLabel := ui.NewLabel("Місце призначення:")
		destinationBox := ui.NewHorizontalBox()
		destinationBox.Append(destinationLabel, false)
		destinationBox.Append(destinationComboBox, true)
		vbox.Append(destinationBox, false)

		// Сезон
		seasonOptions := []string{"літо", "зима"}
		seasonComboBox := ui.NewCombobox()
		for _, option := range seasonOptions {
			seasonComboBox.Append(option)
		}
		seasonComboBox.SetSelected(0) // Задаємо значення за замовчуванням
		seasonLabel := ui.NewLabel("Сезон:")
		seasonBox := ui.NewHorizontalBox()
		seasonBox.Append(seasonLabel, false)
		seasonBox.Append(seasonComboBox, true)
		vbox.Append(seasonBox, false)

		// Чекбокси
		guideCheckbox := ui.NewCheckbox("Додати індивідуального гіда?")
		luxuryCheckbox := ui.NewCheckbox("Проживання в номері люкс?")
		checkboxesBox := ui.NewVerticalBox()
		checkboxesBox.Append(guideCheckbox, false)
		checkboxesBox.Append(luxuryCheckbox, false)
		vbox.Append(checkboxesBox, false)

		// Кнопка розрахунку
		calculateButton := ui.NewButton("Розрахувати")
		resultLabel := ui.NewLabel("")
		calculateButton.OnClicked(func(*ui.Button) {
			destinationIndex := destinationComboBox.Selected()
			seasonIndex := seasonComboBox.Selected()
			guide := guideCheckbox.Checked()
			luxury := luxuryCheckbox.Checked()

			destination := destinationOptions[destinationIndex]
			season := seasonOptions[seasonIndex]

			// Використання текстових значень для обчислення
			price := CalculateTourPrice(destination, season, guide, luxury)
			resultLabel.SetText(fmt.Sprintf("Вартість туру: $%.2f", price))
		})
		vbox.Append(calculateButton, false)
		vbox.Append(resultLabel, false)

		window.SetChild(vbox)

		window.OnClosing(func(*ui.Window) bool {
			ui.Quit()
			return true
		})

		window.Show()
	})
	if err != nil {
		panic(err)
	}
}
