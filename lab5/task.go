package main

import (
	"fmt"
	"strconv"
)

// Структура для валюти
type Currency struct {
	Name   string  // назва валюти
	ExRate float64 // курс обміну
}

// Структура для продукту
type Product struct {
	Name     string  // назва товару
	Price    float64 // вартість одиниці товару
	Cost     Currency
	Quantity int     // кількість товарів на складі
	Producer string  // назва компанії-виробника
	Weight   float64 // вага одиниці товару
}

// Конструктор для Currency
func NewCurrency(name string, exRate float64) *Currency {
	return &Currency{Name: name, ExRate: exRate}
}

// Конструктор для Product
func NewProduct(name string, price float64, cost Currency, quantity int, producer string, weight float64) *Product {
	return &Product{Name: name, Price: price, Cost: cost, Quantity: quantity, Producer: producer, Weight: weight}
}

// Get- та Set- методи для Product

func (p *Product) GetName() string {
	return p.Name
}

func (p *Product) SetName(name string) {
	p.Name = name
}

func (p *Product) GetPrice() float64 {
	return p.Price
}

func (p *Product) SetPrice(price float64) {
	p.Price = price
}

func (p *Product) GetCost() Currency {
	return p.Cost
}

func (p *Product) SetCost(cost Currency) {
	p.Cost = cost
}

func (p *Product) GetQuantity() int {
	return p.Quantity
}

func (p *Product) SetQuantity(quantity int) {
	p.Quantity = quantity
}

func (p *Product) GetProducer() string {
	return p.Producer
}

func (p *Product) SetProducer(producer string) {
	p.Producer = producer
}

func (p *Product) GetWeight() float64 {
	return p.Weight
}

func (p *Product) SetWeight(weight float64) {
	p.Weight = weight
}

// Методи для Product
func (p *Product) GetPriceIn() float64 {
	return p.Price * p.Cost.ExRate
}

func (p *Product) GetTotalPrice() float64 {
	return p.GetPriceIn() * float64(p.Quantity)
}

func (p *Product) GetTotalWeight() float64 {
	return p.Weight * float64(p.Quantity)
}

// Функції
func ReadProductsArray(n int) []*Product {
	products := make([]*Product, n)
	for i := 0; i < n; i++ {
		fmt.Println("Введіть дані про продукт №", i+1)
		var name, producer, currencyName string
		var price, weight, exRate float64
		var quantity int
		fmt.Print("Назва: ")
		fmt.Scanln(&name)
		fmt.Print("Вартість: ")
		fmt.Scanln(&price)
		fmt.Print("Назва валюти (наприклад, USD, EUR): ")
		fmt.Scanln(&currencyName)
		fmt.Print("Курс валюти: ")
		fmt.Scanln(&exRate)
		fmt.Print("Кількість: ")
		fmt.Scanln(&quantity)
		fmt.Print("Виробник: ")
		fmt.Scanln(&producer)
		fmt.Print("Вага одиниці товару: ")
		fmt.Scanln(&weight)
		currency := NewCurrency(currencyName, exRate)
		product := NewProduct(name, price, *currency, quantity, producer, weight)
		products[i] = product
	}
	return products
}

func PrintProduct(p *Product) {
	fmt.Println("Продукт:")
	fmt.Println("Назва:", p.GetName())
	fmt.Println("Вартість:", strconv.FormatFloat(p.GetPrice(), 'f', 2, 64), p.GetCost().Name)
	fmt.Println("Кількість на складі:", p.GetQuantity())
	fmt.Println("Виробник:", p.GetProducer())
	fmt.Println("Вага одиниці товару:", strconv.FormatFloat(p.GetWeight(), 'f', 2, 64))
	fmt.Println("Вартість в гривнях:", strconv.FormatFloat(p.GetPriceIn(), 'f', 2, 64))
	fmt.Println("Загальна вага:", strconv.FormatFloat(p.GetTotalWeight(), 'f', 2, 64))
	fmt.Println("Загальна вартість:", strconv.FormatFloat(p.GetTotalPrice(), 'f', 2, 64))
}

func PrintProducts(products []*Product) {
	for _, p := range products {
		PrintProduct(p)
		fmt.Println("-----------")
	}
}

func GetProductsInfo(products []*Product) (*Product, *Product) {
	var minProduct, maxProduct *Product
	for _, p := range products {
		if minProduct == nil || p.GetPriceIn() < minProduct.GetPriceIn() {
			minProduct = p
		}
		if maxProduct == nil || p.GetPriceIn() > maxProduct.GetPriceIn() {
			maxProduct = p
		}
	}
	return minProduct, maxProduct
}

func main() {
	fmt.Print("Введіть кількість продуктів: ")
	var n int
	fmt.Scanln(&n)
	products := ReadProductsArray(n)
	PrintProducts(products)

	minProduct, maxProduct := GetProductsInfo(products)
	fmt.Println("Найдешевший товар:")
	PrintProduct(minProduct)
	fmt.Println("-----------")
	fmt.Println("Найдорожчий товар:")
	PrintProduct(maxProduct)
}
