package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

type FinancialInstitution struct {
	InstitutionName string
	OwnFunds        float64
	TotalDeposits   float64
	TotalLoans      float64
	AccountHolders  []BankClient
}

type BankClient struct {
	FirstName     string
	LastName      string
	AccNum        int
	DepositAmount float64
	LoanAmount    float64
}

var (
	institutionList []*FinancialInstitution
	clientList      []*BankClient
	accessControl   sync.Mutex
)

func CreateFinancialInstitution(name string) *FinancialInstitution {
	return &FinancialInstitution{InstitutionName: name}
}

func CreateBankClient(firstName, lastName string, accNum int, deposit, loan float64) *BankClient {
	return &BankClient{
		FirstName:     firstName,
		LastName:      lastName,
		AccNum:        accNum,
		DepositAmount: deposit,
		LoanAmount:    loan,
	}
}

func (fi *FinancialInstitution) AdjustFunds(deposit, loan float64) {
	fi.OwnFunds += deposit - loan
}

func (fi *FinancialInstitution) RegisterClient(client *BankClient) {
	fi.AccountHolders = append(fi.AccountHolders, *client)
}

func (bc *BankClient) UpdateDeposit(amount float64) {
	bc.DepositAmount += amount
}

func (bc *BankClient) UpdateLoan(amount float64) {
	bc.LoanAmount -= amount
}

func automatedClientActivity(client *BankClient) {
	for {
		accessControl.Lock()
		// Визначаємо тип операції: 0 - зменшення кредиту, 1 - збільшення депозиту
		operationType := rand.Intn(2)

		if operationType == 0 {
			// Симулюємо виплату частини кредиту
			changeAmount := rand.Float64() * 1000
			if client.LoanAmount-changeAmount <= 0 {
				client.LoanAmount = 0
				fmt.Printf("Клієнт %s %s повністю погасив кредит і завершує роботу.\n", client.FirstName, client.LastName)
				accessControl.Unlock()
				return // Вихід з go-рутини
			} else {
				client.LoanAmount -= changeAmount
				fmt.Printf("Клієнт %s %s зменшив кредит на %.2f. Залишок кредиту: %.2f\n", client.FirstName, client.LastName, changeAmount, client.LoanAmount)
			}
		} else {
			// Симулюємо поповнення депозиту
			changeAmount := rand.Float64() * 1000
			client.DepositAmount += changeAmount
			fmt.Printf("Клієнт %s %s збільшив депозит на %.2f. Залишок на депозиті: %.2f\n", client.FirstName, client.LastName, changeAmount, client.DepositAmount)
			// Перевіряємо, чи клієнт досяг максимального ліміту депозиту
			if client.DepositAmount >= 10000 {
				fmt.Printf("Клієнт %s %s досяг максимального ліміту депозиту і завершує роботу.\n", client.FirstName, client.LastName)
				accessControl.Unlock()
				return // Вихід з go-рутини
			}
		}
		accessControl.Unlock()
		time.Sleep(1 * time.Second)
	}
}

func main() {
	rand.Seed(time.Now().UnixNano())

	for {
		fmt.Println("\nВиберіть операцію:")
		fmt.Println("1 - Створити фінансову установу")
		fmt.Println("2 - Створити клієнта")
		fmt.Println("3 - Переглянути інформацію про клієнта за прізвищем")
		fmt.Println("4 - Переглянути інформацію про клієнта за номером рахунку")
		fmt.Println("5 - Завершити програму")

		var choice int
		fmt.Scan(&choice)

		switch choice {
		case 1:
			createFinancialInstitution()
		case 2:
			createAndStartClientActivity()
		case 3:
			searchClientByLastName()
		case 4:
			searchClientByAccountNumber()
		case 5:
			fmt.Println("Програма завершує свою роботу.")
			return
		default:
			fmt.Println("Невідома команда. Спробуйте ще раз.")
		}
	}
}

func createFinancialInstitution() {
	fmt.Print("Введіть назву фінансової установи: ")
	var name string
	fmt.Scan(&name)
	fi := CreateFinancialInstitution(name)
	institutionList = append(institutionList, fi)
	fmt.Printf("Фінансову установу '%s' успішно створено.\n", name)
}

func createAndStartClientActivity() {
	var firstName, lastName string
	var accNum int
	var deposit, loan float64

	fmt.Print("Введіть ім'я клієнта: ")
	fmt.Scan(&firstName)
	fmt.Print("Введіть прізвище клієнта: ")
	fmt.Scan(&lastName)
	fmt.Print("Введіть номер рахунку: ")
	fmt.Scan(&accNum)
	fmt.Print("Введіть суму на депозиті: ")
	fmt.Scan(&deposit)
	fmt.Print("Введіть суму кредиту: ")
	fmt.Scan(&loan)

	client := CreateBankClient(firstName, lastName, accNum, deposit, loan)
	clientList = append(clientList, client)
	fmt.Printf("Клієнта '%s %s' успішно створено.\n", firstName, lastName)

	go automatedClientActivity(client)
}

func searchClientByLastName() {
	fmt.Print("Введіть прізвище клієнта для пошуку: ")
	var lastName string
	fmt.Scan(&lastName)

	found := false
	for _, client := range clientList {
		if client.LastName == lastName {
			fmt.Printf("Клієнт: %s %s, Рахунок: %d, Депозит: %.2f, Кредит: %.2f\n",
				client.FirstName, client.LastName, client.AccNum, client.DepositAmount, client.LoanAmount)
			found = true
		}
	}
	if !found {
		fmt.Println("Клієнта з таким прізвищем не знайдено.")
	}
}

func searchClientByAccountNumber() {
	fmt.Print("Введіть номер рахунку клієнта для пошуку: ")
	var accNum int
	fmt.Scan(&accNum)

	found := false
	for _, client := range clientList {
		if client.AccNum == accNum {
			fmt.Printf("Клієнт: %s %s, Рахунок: %d, Депозит: %.2f, Кредит: %.2f\n",
				client.FirstName, client.LastName, client.AccNum, client.DepositAmount, client.LoanAmount)
			found = true
			break
		}
	}
	if !found {
		fmt.Println("Клієнта з таким номером рахунку не знайдено.")
	}
}
