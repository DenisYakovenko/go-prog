package main

import (
	"fmt"
	"log"
	"math"
	"net/http"
	"strconv"
)

func main() {
	http.HandleFunc("/solve", solveQuadratic)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func solveQuadratic(w http.ResponseWriter, r *http.Request) {
	var a, b, c float64
	var err error

	if r.Method == "GET" {
		a, err = strconv.ParseFloat(r.URL.Query().Get("a"), 64)
		b, err = strconv.ParseFloat(r.URL.Query().Get("b"), 64)
		c, err = strconv.ParseFloat(r.URL.Query().Get("c"), 64)
		if err != nil {
			fmt.Fprintf(w, formTemplate1)
			return
		}
	} else if r.Method == "POST" {
		err = r.ParseForm()
		a, err = strconv.ParseFloat(r.FormValue("a"), 64)
		b, err = strconv.ParseFloat(r.FormValue("b"), 64)
		c, err = strconv.ParseFloat(r.FormValue("c"), 64)
		if err != nil {
			http.Error(w, "Invalid form data", http.StatusBadRequest)
			return
		}
	}

	discriminant := b*b - 4*a*c
	if discriminant < 0 {
		fmt.Fprintf(w, "No real roots")
		return
	}

	root1 := (-b + math.Sqrt(discriminant)) / (2 * a)
	root2 := (-b - math.Sqrt(discriminant)) / (2 * a)

	fmt.Fprintf(w, "Roots of the equation: %.2f, %.2f", root1, root2)
}

const formTemplate1 = `<!DOCTYPE html>
<html>
<head>
    <title>Solve Quadratic Equation</title>
</head>
<body>
    <h1>Solve Quadratic Equation</h1>
    <form method="POST" action="/solve">
        <input type="text" name="a" placeholder="a" required="required" />
        <input type="text" name="b" placeholder="b" required="required" />
        <input type="text" name="c" placeholder="c" required="required" />
        <input type="submit" value="Solve" />
    </form>
</body>
</html>`
