package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
)

func main() {
	http.HandleFunc("/processSlice", processSlice)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func processSlice(w http.ResponseWriter, r *http.Request) {
	var inputSlice []float64
	var err error

	var input string
	if r.Method == "GET" {
		input = r.URL.Query().Get("slice")
		if input == "" {

			fmt.Fprintf(w, formTemplate)
			return
		}
	} else if r.Method == "POST" {
		err = r.ParseForm()
		if err != nil {
			http.Error(w, "Invalid form data", http.StatusBadRequest)
			return
		}
		input = r.FormValue("slice")
	}

	// Конвертація рядка у зріз дійсних чисел
	for _, v := range strings.Split(input, ",") {
		var num float64
		num, err = strconv.ParseFloat(v, 64)
		if err != nil {
			http.Error(w, "Invalid slice element", http.StatusBadRequest)
			return
		}
		inputSlice = append(inputSlice, num)
	}

	// Виконання розрахунків
	sumOddIndices, productBetweenZeros := calculateOperations(inputSlice)

	// Відображення результатів
	fmt.Fprintf(w, "Sum of elements at odd indices: %.2f\nProduct of elements between first and last zero: %.2f", sumOddIndices, productBetweenZeros)
}

func calculateOperations(slice []float64) (float64, float64) {
	var sumOdd float64
	var product float64 = 1
	var foundFirstZero bool
	var startIndex, endIndex int

	for i, v := range slice {
		if v == 0 {
			if !foundFirstZero {
				foundFirstZero = true
				startIndex = i
			}
			endIndex = i
		}
		if i%2 != 0 {
			sumOdd += v
		}
	}

	if foundFirstZero && startIndex != endIndex {
		for i := startIndex + 1; i < endIndex; i++ {
			product *= slice[i]
		}
	} else {
		product = 0
	}

	return sumOdd, product
}

const formTemplate = `<!DOCTYPE html>
<html>
<head>
    <title>Process Slice</title>
</head>
<body>
    <h1>Enter a Slice of Real Numbers</h1>
    <form method="POST" action="/processSlice">
        <input type="text" name="slice" placeholder="Enter numbers separated by commas, e.g., 1,2,3,0,4" required="required" />
        <input type="submit" value="Process" />
    </form>
</body>
</html>`
